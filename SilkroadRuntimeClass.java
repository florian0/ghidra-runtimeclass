//Find all runtimeclass assets based on the ctor of CGfxRuntimeClass
//@author florian0
//@category _SRO
//@keybinding 
//@menupath 
//@toolbar 

import ghidra.app.cmd.data.CreateDataCmd;
import ghidra.app.cmd.function.CreateFunctionCmd;
import ghidra.app.script.GhidraScript;
import ghidra.program.flatapi.FlatProgramAPI;
import ghidra.program.model.util.*;
import ghidra.program.model.reloc.*;
import ghidra.program.model.data.*;
import ghidra.program.model.block.*;
import ghidra.program.model.symbol.*;
import ghidra.program.model.scalar.*;
import ghidra.program.model.mem.*;
import ghidra.program.model.listing.*;
import ghidra.program.model.lang.*;
import ghidra.program.model.pcode.*;
import ghidra.program.model.address.*;
import ghidra.program.model.correlate.*;

public class SilkroadRuntimeClass extends GhidraScript {

    // TODO: Make this step by instructions, not bytes
    private Address findFunctionStart(Address searchAddr) throws MemoryAccessException {
        var mem = currentProgram.getMemory();

        for (int i = 0; i < 40; i++) {
            // Search upwards until 0xCC is found ... maybe ...

            byte b = mem.getByte(searchAddr);

            if (b != (byte) 0xcc) {
                searchAddr = searchAddr.subtract(1);
            } else {
                long addr = searchAddr.getOffset() + 1;
                long rem = addr % 16;

                if (rem == 0) {
                    return searchAddr.add(1);
                }

                searchAddr = searchAddr.subtract(1);
            }
        }

        return Address.NO_ADDRESS;
    }

    // Apply name and namespace to a function
    // Create the function at given address if its not existing
    private Function makeFnIfNotExists(Address addr, Namespace namespace, String name) throws Exception {
        var fun = currentProgram.getFunctionManager().getFunctionAt(addr);

        if (fun == null) {
            CreateFunctionCmd cmd = new CreateFunctionCmd(addr, true);
            cmd.applyTo(currentProgram);

            fun = currentProgram.getListing().getFunctionAt(addr);
        }

        fun.setName(name, SourceType.ANALYSIS);
        fun.setParentNamespace(namespace);

        return fun;
    }

    // Process address of Create-function
    private void processCreateFN(Address addr, Namespace namespace) throws Exception {
        if (addr.getOffset() == 0)
            return;

        Function fn = makeFnIfNotExists(addr, namespace, "Create");
    }

    // Process address of Delete-function
    private void processDeleteFN(Address addr, Namespace namespace) throws Exception {
        if (addr.getOffset() == 0)
            return;

        Function fn = makeFnIfNotExists(addr, namespace, "Delete");
    }

    private void tryLabelGetRuntimeClass(String name, Address objAddr, Namespace namespace) throws Exception {
        for (var rtclassref : currentProgram.getReferenceManager().getReferencesTo(objAddr)) {
            Address from = rtclassref.getFromAddress();
            Instruction inst = currentProgram.getListing().getInstructionAt(from);
            if (inst == null)
                continue;

            if (inst.getMnemonicString().compareToIgnoreCase("MOV") != 0)
                continue;

            if (inst.getNumOperands() != 2)
                continue;

            if (inst.getDefaultOperandRepresentation(0).compareToIgnoreCase("eax") != 0)
                continue;

            //printf("%08x - %s : %d\n", from.getOffset(), inst.toString(), inst.getNumOperands());

            Function fn = currentProgram.getFunctionManager().getFunctionContaining(from);
            fn.setName(name, SourceType.ANALYSIS);
            fn.setParentNamespace(namespace);
        }
    }


    // Process pointer to runtime class structure
    private void processRuntimeClassPtr(Address addr, Namespace namespace) throws Exception {
        // Assign name
        String name = "class" + namespace.getName();
        currentProgram.getSymbolTable().createLabel(addr, name, namespace, SourceType.ANALYSIS);

        // Create data and type
        DataTypePath path = new DataTypePath(CategoryPath.ROOT, "CGfxRuntimeClass");
        var dataType = currentProgram.getDataTypeManager().getDataType(path);
        if (dataType == null)
            throw new Exception("Datatype not found");

        CreateDataCmd createData = new CreateDataCmd(addr, dataType);
        createData.applyTo(currentProgram);
    }

    // Process size of the actual class
    private void processSize(long size, Namespace namespace) {
        // Setting the size is currently not implemented ...
    }


    public void run() throws Exception {
        var funmgr = currentProgram.getFunctionManager();
        var addrfact = currentProgram.getAddressFactory();
        var listing = currentProgram.getListing();
        var refman = currentProgram.getReferenceManager();

        // Address to the GfxRuntimeClass constructor
        Address address = addrfact.getAddress("0x00b9c9c0");

        // Get references to the constructor (should all be static initializers ...
        var references = refman.getReferencesTo(address);

        monitor.setMaximum(refman.getReferenceCountTo(address) - 1);

        int progress = 0;

        // Walk through all references
        for (var ref : references) {
            monitor.setProgress(progress++);

            Address currentFromAddress = ref.getFromAddress();
            println("Processing " + currentFromAddress.toString());
            Function currentFunction = funmgr.getFunctionContaining(currentFromAddress);
            // Create a function for the static initializer, if not already present
            if (currentFunction == null) {
                println("Function does not exist");

                Address start = findFunctionStart(currentFromAddress);

                if (start == Address.NO_ADDRESS) {
                    throw new Exception("Could not find start for " + currentFromAddress.toString());
                } else {
                    printf("Start is at %08x\n", start.getOffset());
                }

                CreateFunctionCmd cmd = new CreateFunctionCmd(start, true);
                cmd.applyTo(currentProgram);

                currentFunction = funmgr.getFunctionAt(start);
            }

            // Get instruction backward iterator
            var instructionIter = listing.getInstructions(currentFromAddress, false);

            boolean bExit = false;
            int count = 0;


            Address obj = null;
            Address objName = null;
            Address createFn = null;
            Address deleteFn = null;
            Address parent = null;
            long size = 0;
            int numberOfArgsLeft = 6;

            // Navigate backwards, instructionwise
            // Parse each instruction to extract the parameters of the constructor-call
            while (instructionIter.hasNext() && !monitor.isCancelled() && !bExit && numberOfArgsLeft > 0) {
                Instruction instruction = instructionIter.next();
                String mnemonic = instruction.getMnemonicString();

                if (mnemonic.compareTo("CALL") == 0) {
                    // We're at the call site, ignore
                } else if (mnemonic.compareTo("MOV") == 0) {
                    // Now we got the object, probably
                    if (instruction.getNumOperands() != 2) {
                        printerr("Unexpected number of operants");
                        return;
                    }

                    if (instruction.getDefaultOperandRepresentation(0).compareTo("ECX") != 0) {
                        printerr("Unexpected operant");
                        return;
                    }

                    if (obj != null) {
                        printerr("Unexpected objAddr");
                        return;
                    }

                    String objAddr = instruction.getDefaultOperandRepresentation(1);
                    obj = addrfact.getAddress(objAddr);


                } else if (mnemonic.compareTo("PUSH") == 0) {
                    switch (numberOfArgsLeft--) {
                        case 6: // NAME
                            String objNameAddr = instruction.getDefaultOperandRepresentation(0);
                            objName = addrfact.getAddress(objNameAddr);
                            break;

                        case 5: // CREATE FN
                            String createFnAddr = instruction.getDefaultOperandRepresentation(0);
                            createFn = addrfact.getAddress(createFnAddr);
                            break;

                        case 4: // DELETE FN
                            String deleteFnAddr = instruction.getDefaultOperandRepresentation(0);
                            deleteFn = addrfact.getAddress(deleteFnAddr);
                            break;

                        case 3: // PARENT
                            String parentAddr = instruction.getDefaultOperandRepresentation(0);
                            parent = addrfact.getAddress(parentAddr);
                            break;

                        case 2: // SIZE
                            String sizeStr = instruction.getDefaultOperandRepresentation(0);
                            size = parseLong(sizeStr);
                            break;

                        case 1: // irrelevant
                            // drop this
                            break;
                    }
                }

                // Just a safety measure to avoid parsing-runaway
                if (count++ > 10) {
                    bExit = true;
                }

            }

            // Get the class name for this runtime class
            Data namespaceNameData = listing.getDataAt(objName);
            if (namespaceNameData == null)
                throw new Exception("namespaceNameData not found " + objName.toString());

            // Get the namespace to the class name
            String namespaceName = namespaceNameData.getDefaultValueRepresentation().replaceAll("^\"|\"$", ""); // Remove quotes
            Namespace namespace = currentProgram.getSymbolTable().getNamespace(namespaceName, currentProgram.getGlobalNamespace());

            if (namespace == null)
                throw new Exception("Namespace '" + namespaceName + "'was not found");

            tryLabelGetRuntimeClass("GetRuntimeClass", obj, namespace);
            // TODO: Read the 12 byte offset from actual structure
            tryLabelGetRuntimeClass("GetParentRuntimeClass", obj.add(12), namespace);

            // Apply namespace and name to static initializer function
            currentFunction.setParentNamespace(namespace);
            currentFunction.setName("cinit_runtimeclass", SourceType.ANALYSIS);

            // Pass extracted parameters of constructor-call to processing-functions
            processCreateFN(createFn, namespace);
            processDeleteFN(deleteFn, namespace);
            processRuntimeClassPtr(obj, namespace);
            processSize(size, namespace);
        }

    }

}
