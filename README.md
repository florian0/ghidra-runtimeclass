# Ghidra Run-Time Class

Simple script to automatically analyze a Run-Time Class structure quite similar 
to Run-Time Classes in MFC:

* https://docs.microsoft.com/en-us/cpp/mfc/reference/run-time-object-model-services?view=vs-2005
* https://docs.microsoft.com/en-us/cpp/mfc/accessing-run-time-class-information?view=vs-2005

![Demo Screenshot](screenshot.png)

## Features

* Extracts class name from Run-Time Class Information
* Places labels on `Create`, `Delete` and static initializer functions
* Places label on current Run-Time Class pointer

## Limitations

* Can not find CGfxRuntimeClass-constructor
* Can not deal with some static-initializer functions Ghidra wasn't able to find before.
* Can not deal with incorrectly aligned strings (class name in Run-Time Class Information).

